# Setup vim for editing yaml



## Getting started

Requires Ansible to be installed.

```
git clone https://gitlab.com/rgdacosta/vim-yaml.git

cd vim-yaml

ansible-playbook setup_editor.yml
```

